<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '1556354047708423',
        'client_secret' => 'd8186987189ca1638ec230c79562cdaf',
        'redirect' => 'http://localhost/login/facebook/callback',
    ],

    'google' => [
        'client_id' => '75657812143-95rsirtuudhib33rab8agqio0f13f3e4.apps.googleusercontent.com',
        'client_secret' => 'VEXNm_fn28uMp5lfkLiyTYxp',
        'redirect' => 'http://localhost/login/google/callback',
    ],

];
