@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Create a character</h3></div>
                    <div class="panel-body">
                        {!! Form::open(['url' => '/character/create']) !!}
                        <div class="form-group">
                            {{ Form::label('name', 'Character name(s)') }}
                            {{ Form::text('name', null, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('country', 'Home country') }}
                            {{ Form::text('country', null, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('region', 'Home region') }}
                            {{ Form::text('region', null, ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('gender', 'Gender') }}
                            {{ Form::select('gender', ['M' => 'Male', 'F' => 'Female'], 'M', ['class' => 'form-control']) }}
                        </div>
                        <input class="btn btn-default" type="submit" value="Submit">
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
